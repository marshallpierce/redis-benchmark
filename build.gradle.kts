import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.21"
    id("com.github.ben-manes.versions") version "0.20.0"
}

val deps by extra {
    mapOf(
            "ktor" to "1.1.3",
            "jackson" to "2.9.8",
            "slf4j" to "1.7.26"
    )
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.ktor:ktor-server-core:${deps["ktor"]}")
    implementation("io.ktor:ktor-server-netty:${deps["ktor"]}")
    implementation("io.ktor:ktor-jackson:${deps["ktor"]}")

    implementation("com.fasterxml.jackson.core:jackson-databind:${deps["jackson"]}")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:${deps["jackson"]}")

    runtime("ch.qos.logback:logback-classic:1.2.3")
    runtime("org.slf4j:jcl-over-slf4j:${deps["slf4j"]}")
    implementation("org.slf4j:jul-to-slf4j:${deps["slf4j"]}")

    implementation("com.google.inject:guice:4.2.2")

    implementation("io.lettuce:lettuce-core:5.1.6.RELEASE")
    
    implementation("org.asynchttpclient:async-http-client:2.8.1")

    implementation("org.hdrhistogram:HdrHistogram:2.1.11")
}

configurations.all {
    // don't let commons logging creep into the classpath; use jcl-over-slf4j instead
    exclude("commons-logging", "commons-logging")
}

// compile to java 8 bytecode, not java 6
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.jvmTarget = "1.8"
