package org.mpierce.redis

import org.HdrHistogram.Recorder
import org.asynchttpclient.Dsl.asyncHttpClient
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.Semaphore
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger


private val logger = LoggerFactory.getLogger("org.mpierce.redis.http")

fun main() {
    logger.info("Starting warmup")

    asyncHttpClient().use { http ->
        val ses = Executors.newScheduledThreadPool(2)
        val maxId = 100_000

        // warm up
        doScheduledWorkload(ses, 50_000) { onRequestDone ->
            http.prepareGet("http://localhost:8080/things/id/${ThreadLocalRandom.current().nextInt(0, maxId)}")
                    .execute()
                    .toCompletableFuture()
                    .whenComplete { r, t ->
                        if (t == null) {
                            if (r.statusCode != 200) {
                                logger.warn("bad status: $r")
                            }
                        } else {
                            logger.warn("request failed", t)
                        }
                        onRequestDone()
                    }
        }

        logger.info("Warmup done")

        val recorder = Recorder(1_000_000_000, 3)

        doScheduledWorkload(ses, 100_000) { onRequestDone ->
            val start = System.nanoTime()
            http.prepareGet("http://localhost:8080/things/id/${ThreadLocalRandom.current().nextInt(0, maxId)}")
                    .execute()
                    .toCompletableFuture()
                    .whenComplete { r, t ->
                        val end = System.nanoTime()
                        if (t == null) {
                            if (r.statusCode != 200) {
                                logger.warn("bad status: $r")
                            } else {
                                recorder.recordValue(end - start)
                            }
                        } else {
                            logger.warn("request failed", t)
                        }
                        onRequestDone()
                    }
        }

        logger.info("Real work done")
        recorder.intervalHistogram.outputPercentileDistribution(System.out, 1_000_000.0)

    }
}

fun doScheduledWorkload(ses: ScheduledExecutorService, numRequests: Int, doRequest: (() -> Unit) -> Unit) {
    val counter = AtomicInteger()
    val done = Semaphore(0)
    val future = ses.scheduleAtFixedRate({
        doRequest {
            if (counter.incrementAndGet() == numRequests) {
                done.release()
            }
        }
    }, 0, 1, TimeUnit.MILLISECONDS)

    done.acquire()
    future.cancel(false)
}
