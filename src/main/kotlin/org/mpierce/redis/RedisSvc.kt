package org.mpierce.redis

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Inject
import com.google.inject.Injector
import com.google.inject.Module
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.Stage
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.jackson.JacksonConverter
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.lettuce.core.RedisClient
import io.lettuce.core.RedisURI
import io.lettuce.core.api.StatefulRedisConnection
import io.lettuce.core.codec.StringCodec
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import java.nio.charset.StandardCharsets
import java.time.Duration
import java.time.Instant
import java.util.concurrent.Callable
import java.util.concurrent.Executors

fun main(args: Array<String>) {
    val start = Instant.now()
    // Use SLF4J for java.util.logging
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()

    // Help the service start up as fast as possible by initializing a few slow things in different
    // threads. If using threads is confusing, just remove the thread pool and do things serially.
    val initPool = Executors.newCachedThreadPool()

    val jackson = initPool.submit(Callable<ObjectMapper> {
        configuredObjectMapper()
    })

    val logger = LoggerFactory.getLogger("org.mpierce.redis")

    val lettuceClient = initPool.submit(Callable<StatefulRedisConnection<String, String>> {
        lettuceClient()
    })

    // This is totally optional but it helps the service start faster by having classes already loaded
    // by the time they're needed.
    val otherWarmupFutures = listOf(
            initPool.submit(::warmUpGuice)
    )

    val server = embeddedServer(Netty, port = 8080) {
        // any other ktor features would be set up here
        configureJackson(this, jackson.get())
        setupGuice(this,
                JacksonModule(jackson.get()),
                LettuceThingStoreModule(lettuceClient.get()))

        otherWarmupFutures.forEach { it.get() }
        logger.info("Server initialized in ${Duration.between(start, Instant.now())}")
    }
    initPool.shutdown()
    server.start(wait = true)
}

fun setupGuice(app: Application, vararg modules: Module): Injector {
    return Guice.createInjector(Stage.PRODUCTION,
            object : AbstractModule() {
                override fun configure() {
                    modules.forEach { m -> install(m) }

                    bind(Application::class.java).toInstance(app)

                    // endpoints get bound eagerly so routes are set up
                    listOf(ThingEndpoints::class.java)
                            .forEach { bind(it).asEagerSingleton() }

                    install(GuiceConfigModule())
                }
            })
}

fun configureJackson(app: Application, objectMapper: ObjectMapper) {
    app.install(ContentNegotiation) {
        register(ContentType.Application.Json, JacksonConverter(objectMapper))
    }
}

fun configuredObjectMapper(): ObjectMapper {
    return ObjectMapper().apply {
        // Write dates in ISO8601
        setConfig(serializationConfig.withoutFeatures(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS))
        // Don't barf when deserializing json with extra stuff in it
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        // Kotlin support
        registerModule(KotlinModule())
    }
}

fun lettuceClient(): StatefulRedisConnection<String, String> {
    return RedisClient.create(RedisURI.Builder
            .redis("127.0.0.1")
            .withPort(6379)
            .withTimeout(Duration.ofSeconds(2))
            .build())
            .connect(StringCodec(StandardCharsets.UTF_8))
}

/**
 * Warm up Guice classes before we actually have the final set of modules available to inject.
 * This saves a few hundred ms for overall app startup.
 */
private fun warmUpGuice() {
    class Dependent
    @Suppress("unused")
    class Depender @Inject constructor(private val dependent: Dependent)

    // warming up Guice pays down about 200ms towards creating the final injector,
    // which would otherwise typically be the last thing to finish (on Macbook Pro)
    val injector = Guice.createInjector(Stage.PRODUCTION, object : AbstractModule() {
        override fun configure() {
            install(GuiceConfigModule())
            bind(Depender::class.java)
        }

        // use a provides method to warm up more reflection stuff
        @Provides
        @Singleton
        fun getDependent(): Dependent = Dependent()
    })

    injector.getInstance(Depender::class.java)
}

/**
 * Sane base Guice config
 */
class GuiceConfigModule : AbstractModule() {
    override fun configure() {
        binder().requireAtInjectOnConstructors()
        binder().requireExactBindingAnnotations()
        binder().requireExplicitBindings()
    }
}

class JacksonModule(private val mapper: ObjectMapper) : AbstractModule() {
    @Provides
    fun reader(): ObjectReader = mapper.reader()

    @Provides
    fun writer(): ObjectWriter = mapper.writer()
}
