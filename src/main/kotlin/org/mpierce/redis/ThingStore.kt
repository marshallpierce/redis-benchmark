package org.mpierce.redis

import java.io.Closeable
import java.util.concurrent.CompletionStage

interface ThingStore : Closeable {

    fun get(id: Int): CompletionStage<Thing?>

    fun put(id: Int, thing: Thing): CompletionStage<Unit>

}

data class Thing(val data: String)
