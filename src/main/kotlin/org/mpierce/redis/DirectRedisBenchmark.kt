package org.mpierce.redis

import org.HdrHistogram.Recorder
import org.slf4j.LoggerFactory
import java.util.concurrent.Executors
import java.util.concurrent.ThreadLocalRandom


private val logger = LoggerFactory.getLogger("org.mpierce.redis.http")

fun main() {
    logger.info("Starting warmup")

    lettuceClient().use { lettuce ->
        val ses = Executors.newScheduledThreadPool(2)
        val maxId = 100_000

        val redis = lettuce.async()

        // warm up
        doScheduledWorkload(ses, 50_000) { onRequestDone ->
            redis.get(ThreadLocalRandom.current().nextInt(0, maxId).toString())
                    .whenComplete { r, t ->
                        if (t == null) {
                            if (r == null) {
                                logger.warn("not found")
                            }
                        } else {
                            logger.warn("request failed", t)
                        }
                        onRequestDone()
                    }
        }

        logger.info("Warmup done")

        val recorder = Recorder(1_000_000_000, 3)

        doScheduledWorkload(ses, 100_000) { onRequestDone ->
            val start = System.nanoTime()
            redis.get(ThreadLocalRandom.current().nextInt(0, maxId).toString())
                    .whenComplete { r, t ->
                        val end = System.nanoTime()
                        if (t == null) {
                            if (r == null) {
                                logger.warn("not found")
                            } else {
                                recorder.recordValue(end - start)
                            }
                        } else {
                            logger.warn("request failed", t)
                        }
                        onRequestDone()
                    }
        }

        logger.info("Real work done")
        recorder.intervalHistogram.outputPercentileDistribution(System.out, 1_000_000.0)
    }

}
