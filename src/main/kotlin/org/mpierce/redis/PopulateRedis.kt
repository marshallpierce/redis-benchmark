package org.mpierce.redis

import com.google.inject.Guice

fun main() {
    Guice.createInjector(
            GuiceConfigModule(),
            JacksonModule(configuredObjectMapper()),
            LettuceThingStoreModule(lettuceClient())
    )
            .getInstance(ThingStore::class.java)
            .use { store ->
                repeat(100_000) { id ->
                    store.put(id, Thing("foo bar baz qwerty asdf"))
                            .toCompletableFuture()
                            .get()
                }
            }
}
