package org.mpierce.redis

import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.databind.ObjectWriter
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import io.lettuce.core.api.StatefulRedisConnection
import java.util.concurrent.CompletionStage

class LettuceThingStore(private val redisConn: StatefulRedisConnection<String, String>,
                        r: ObjectReader,
                        w: ObjectWriter) : ThingStore {
    private val reader: ObjectReader = r.forType(Thing::class.java)
    private val writer: ObjectWriter = w.forType(Thing::class.java)
    private val asyncRedis = redisConn.async()

    override fun get(id: Int): CompletionStage<Thing?> {
        return asyncRedis.get(id.toString())
                .thenApply { str -> str?.let { reader.readValue<Thing>(it) } }
    }

    override fun put(id: Int, thing: Thing): CompletionStage<Unit> {
        return asyncRedis.set(id.toString(), writer.writeValueAsString(thing))
                .thenApply { }
    }

    override fun close() {
        redisConn.close()
    }
}

class LettuceThingStoreModule(private val redis: StatefulRedisConnection<String, String>) : AbstractModule() {

    @Singleton
    @Provides
    fun thingStore(reader: ObjectReader, writer: ObjectWriter): ThingStore {
        return LettuceThingStore(redis, reader, writer)
    }
}
