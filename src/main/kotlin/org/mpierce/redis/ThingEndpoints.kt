package org.mpierce.redis

import com.google.inject.Inject
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import kotlinx.coroutines.future.await
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ThingEndpoints @Inject constructor(app: Application, private val thingStore: ThingStore) {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(ThingEndpoints::class.java)
    }

    init {
        app.routing {
            get("/things/id/{id}") {
                val id = call.parameters["id"]!!.toInt()
                val t = thingStore.get(id).await()

                when (t) {
                    null -> call.respond(HttpStatusCode.NotFound)
                    else -> call.respond(t)
                }
            }
        }
    }
}
